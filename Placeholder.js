import React from 'react';
import PropTypes from 'prop-types';

import ComponentInfo from 'component-info';

import './Placeholder.scss';

/**
 * Placeholder
 * @description [Description]
 * @example
  <div id="Placeholder"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Placeholder, {
    	title : 'Example Placeholder'
    }), document.getElementById("Placeholder"));
  </script>
 */
class Placeholder extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'placeholder';
	}

	render() {
		return (
			<div className={`${this.baseClass}`}>
				<ComponentInfo isTabular={true} {...this.props} />
			</div>
		);
	}
}

Placeholder.defaultProps = {};

Placeholder.propTypes = {};

export default Placeholder;
